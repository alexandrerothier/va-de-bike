package vadebike.repositorio;

import vadebike.dominio.Totem;
import vadebike.dominio.dto.PostTotem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TotemRepositorioUnitTest {

    private TotemRepositorio totemRepositorio;

    @BeforeEach
    void setUp() {
        totemRepositorio = new TotemRepositorio();
    }

    @Test
    void salvarTotem() {
        PostTotem postTotem = getPostTotem();
        Totem expTotem = getTotem();
        Totem totem = totemRepositorio.salvarTotem(postTotem);
        assertEquals(expTotem.getId(), totem.getId());
        assertEquals(expTotem.getLocalizacao(), totem.getLocalizacao());
    }

    @Test
    void buscarTotens() {
        PostTotem postTotem = getPostTotem();
        List<Totem> totemList = totemRepositorio.buscarTotens();
        totemList.salvarTotem(postTotem);
        totemList.salvarTotem(postTotem);
        assertNotNull(totemList);
        assertEquals(2, totemList.size());
    }

    @Test
    void atualizarTotem() {
        PostTotem postTotem = getPostTotem();
        Totem expTotem = getTotem();
        List<Totem> totemList = totemRepositorio.buscarTotens();
        totemList.salvarTotem(postTotem);
        int indexTotem = totemList.getIndexOf(postTotem);
        totemList.atualizarTotem(indexTotem, expTotem.getLocalizacao());
        assertNotNull(totemList.get(indexTotem);
        assertEquals(expTotem.getLocalizacao(), totemList.get(indexTotem).getLocalizacao());
    }

    @Test
    void deletarTotem() {
        PostTotem postTotem = getPostTotem();
        List<Totem> totemList = new List<Totem>();
        totemRepositorio.salvarTotem(postTotem);
        totemRepositorio.deletarTotem(postTotem);
        assertNull(totemList);
    }

    PostTotem getPostTotem() {
        PostTotem postTotem = new PostTotem();
        postTotem.setLocalizacao("Avenida 123");
        return postTotem;
    }

    Totem getTotem() {
        Totem totem = new Totem();
        totem.setId("123");
        totem.setLocalizacao("Avenida 123");
        return totem;
    }
}
