package vadebike.controller;

import vadebike.dominio.Totem;
import vadebike.dominio.dto.PostTotem;
import vadebike.util.JavalinApp;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TotemControllerIntegrationTest {

    private static final JavalinApp javalinApp = new JavalinApp();

    @BeforeAll
    static void init() {
        javalinApp.start(7010);
    }

    @AfterAll
    static void afterAll() {
        javalinApp.stop();
    }

    @Test
    void salvar() {
        PostTotem postTotem = getPostTotem();
        Totem totem = getTotem();
        String json = JavalinJson.toJson(postTotem);
        String expJson = JavalinJson.toJson(totem);
        HttpResponse<String> response = Unirest.post("http://localhost:7010/totem")
                .body(json).asString();
        Assertions.assertEquals(201, response.getStatus());
        Assertions.assertEquals(expJson, response.getBody());
    }

    PostTotem getPostTotem() {
        PostTotem postTotem = new PostTotem();
        postTotem.setLocalizacao("Avenida 123");
        return postTotem;
    }

    Totem getTotem() {
        Totem totem = new Totem();
        totem.setId("123");
        totem.setLocalizacao("Avenida 123");
        return totem;
    }
}
