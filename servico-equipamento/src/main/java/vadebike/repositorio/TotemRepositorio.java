package vadebike.repositorio;

import vadebike.dominio.Totem;
import vadebike.dominio.dto.PostTotem;

import java.util.LinkedList;
import java.util.List;

public class TotemRepositorio {

    private final List<Totem> totemList = new LinkedList<>();

    public Totem salvarTotem(PostTotem postTotem) {
        Totem totem = new Totem(totemList.length(), postTotem.getLocalizacao());
        totemList.add(totem);
        return totem;
    }

    public List<Totem> buscarTotens() { return totemList; }

    public static void atualizarTotem(String id, String localizacao){
        if (totemList.contains(id)){
            totemList.get(totemList.getIndexOf(id)).setLocalizacao(localizacao);
            print("Atualizado.");
            return;
        }
        print("Item não existe.");
        return;
    }

    public static void deletarTotem(String id){
        if (totemList.contains(id)){
            totemList.get(totemList.getIndexOf(id)).remove();
            print("Atualizado.");
            return;
        }
        print("Item não existe.");
        return;
    }
}
