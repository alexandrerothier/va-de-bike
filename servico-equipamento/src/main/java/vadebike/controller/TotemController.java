package vadebike.controller;

import vadebike.dominio.Totem;
import vadebike.dominio.dto.PostTotem;
import vadebike.repositorio.TotemRepositorio;
import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;

import java.util.List;

public class TotemController {

    public static TotemRepositorio totemRepositorio = new TotemRepositorio();

    private TotemController(){}

    public static void salvarTotem(Context ctx) {
        String body = ctx.body();
        PostTotem postTotem = JavalinJson.getFromJsonMapper().map(body, PostTotem.class);
        Totem totem = totemRepositorio.salvarTotem(postTotem);
        String response = JavalinJson.toJson(totem);
        ctx.result(response);
        ctx.status(201);
    }

    public static void buscarTotens(Context ctx) {
        List<Totem> totemList = totemRepositorio.buscarTotens();
        String response = JavalinJson.toJson(totemList);
        ctx.result(response);
        ctx.status(200);
    }

    public static void atualizarTotem(Context ctx) {
        List<Totem> totemList = totemRepositorio.buscarTotens();
        String body = ctx.body();
        PostTotem postTotem = JavalinJson.getFromJsonMapper().map(body, PostTotem.class);
        Totem totem = totemList.salvarTotem(postTotem);
        totemList.atualizarTotem(totem.getId(), totem.getLocalizacao());
        String response = JavalinJson.toJson(totemList);
        ctx.result(response);
        ctx.status(200);
    }

    public static void deletarTotem(Context ctx) {
        List<Totem> totemList = totemRepositorio.buscarTotens();
        String body = ctx.body();
        PostTotem postTotem = JavalinJson.getFromJsonMapper().map(body, PostTotem.class);
        if(totemList.contains(postTotem)) {
            totemList.deletarTotem(postTotem);
            ctx.status(200);
        } else {
            ctx.status(400);
        }
    }
}
