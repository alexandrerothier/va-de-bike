package vadebike.dominio.dto;


public class PostTotem {
    private String localizacao;

    public PostTotem() {
    }

    public PostTotem(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }
}
