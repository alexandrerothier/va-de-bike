package vadebike.util;

import vadebike.controller.TotemController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                    path("/totem", () -> get(TotemController::buscarTotens));
                    path("/totem", () -> post(TotemController::salvarTotem));
                    path("/totem", () -> put(TotemController::atualizarTotem);
                    path("/totem", () -> delete(TotemController::deletarTotem));
                    });
                    


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
