package vadebike.unidade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import vadebike.dominio.Email;
import vadebike.servico.ServicoEmail;

public class ServicoEmailUnitTests {

    private final ServicoEmail servicoEmail = new ServicoEmail();

    @Test
    void enviaEmail_QuandoBemSucedido() {
        Email email = createEmail();
        Email emailResponse = Assertions.assertDoesNotThrow(() -> servicoEmail.enviarMensagem(email));
        Assertions.assertEquals(email.getEmail(), emailResponse.getEmail());
        Assertions.assertEquals(email.getMensagem(), emailResponse.getMensagem());
        Assertions.assertNotNull(emailResponse.getId());
    }

    Email createEmail() {
        Email email = new Email();
        email.setEmail("pedroclain@edu.unirio.br");
        email.setMensagem("Testando");
        return email;
    }
}
