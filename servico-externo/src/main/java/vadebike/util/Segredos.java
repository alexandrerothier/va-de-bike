package vadebike.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Segredos {
    private static final FileInputStream fileInputStream;
    private static final Properties properties;
    private static final String EMAIL_API_KEY = "EMAIL_API_KEY";
    private static final String EMAIL_DOMAIN = "EMAIL_DOMAIN";

    static {
        try {
            fileInputStream = new FileInputStream("segredos.properties");
            properties = new Properties();
            properties.load(fileInputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static String getEmailApiKey() {
        String emailApiKey = System.getenv(EMAIL_API_KEY);
        if (emailApiKey == null) emailApiKey = properties.getProperty(EMAIL_API_KEY);
        return emailApiKey;
    }

    public static String getEmailDomainName() {
        String emailDomainName = System.getenv(EMAIL_DOMAIN);
        if (emailDomainName == null) emailDomainName = properties.getProperty(EMAIL_DOMAIN);
        return emailDomainName;
    }
}
