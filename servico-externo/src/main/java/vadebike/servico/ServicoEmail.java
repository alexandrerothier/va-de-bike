package vadebike.servico;

import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.client.MailgunClient;
import com.mailgun.model.message.Message;
import com.mailgun.model.message.MessageResponse;
import feign.Retryer;
import vadebike.dominio.Email;
import vadebike.util.Segredos;

public class ServicoEmail {

    public Email enviarMensagem(Email email) {
        MailgunMessagesApi mailgunMessagesApi = MailgunClient.config(Segredos.getEmailApiKey())
                .createApi(MailgunMessagesApi.class);

        Message message = Message.builder()
                .from("noreply@vadebike.com")
                .to(email.getEmail())
                .subject("Va de bike")
                .text(email.getMensagem())
                .build();
        mailgunMessagesApi.sendMessage(Segredos.getEmailDomainName(), message);
        // TODO Gerar id
        email.setId("123");
        return email;
    }
}
